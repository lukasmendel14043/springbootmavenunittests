package com.example.demotestproject;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class MathTests {

    Maths maths = new Maths();

    @Test
    public void multiplyNumbersTest(){

        int result = maths.multiplyNumbers(2,4);
        assertEquals(8, result);
    }

    @Test
    public void addNumbersTest(){

        int result = maths.addNumbers(3,4);
        assertEquals(7, result);
    }

}
