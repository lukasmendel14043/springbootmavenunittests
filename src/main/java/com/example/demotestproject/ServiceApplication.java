package com.example.demotestproject;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServiceApplication {


    @RequestMapping("/")
    public String home(){

        return "Test result";
    }
}
