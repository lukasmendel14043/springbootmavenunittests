package com.example.demotestproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class DemoTestProjectApplication {

    public static void main(String[] args) {

        SpringApplication.run(DemoTestProjectApplication.class, args);
    }

}
